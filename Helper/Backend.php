<?php

namespace BrainIndustries\Core\Helper;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Framework\App\State as AppState;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Model\Order\Status\HistoryFactory;

class Backend extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var AppState
     */
    private $_appState;

    /**
     * @var AuthSession
     */
    private $_authSession;

    /**
     * @var HistoryFactory
     */
    private $_historyFactory;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    private $_historyRepository;

    /**
     * @param AppState $appState
     * @param AuthSession $authSession
     * @param HistoryFactory $historyFactory
     * @param OrderStatusHistoryRepositoryInterface
     * @param Context $context
     */
    public function __construct(
        AppState $appState,
        AuthSession $authSession,
        HistoryFactory $historyFactory,
        OrderStatusHistoryRepositoryInterface $historyRepository,
        Context $context
        ) {
            parent::__construct($context);
            $this->_appState = $appState;
            $this->_authSession = $authSession;
            $this->_historyFactory = $historyFactory;
            $this->_historyRepository = $historyRepository;
        }

    /**
     * Add comment to order, optionally with name of the admin user, or cron if
     * the request is done by a cronjob
     * 
     * @access public
     * @param mixed $order
     * @param mixed $comment     
     * @param bool $addAdminName
     * @return \Magento\Sales\Api\Data\OrderStatusHistoryInterface
     */
    public function addOrderComment($order, $comment, $addAdminName = true)
    {        
        if ($addAdminName) {
            if ($this->_appState->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML) {
                $adminName = $this->_authSession->getUser()->getName();
                $comment .= ' by ' . $adminName;
            } elseif ($this->_appState->getAreaCode() === \Magento\Framework\App\Area::AREA_CRONTAB) {
                $comment .= ' by cron';
            }
        }

        $history = $this->_historyFactory->create()
                ->setStatus($order->getStatus())
                ->setParentId($order->getId())
                ->setComment($comment)
                ->setEntityName('order');

        return $this->_historyRepository->save($history);
    }
}