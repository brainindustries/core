<?php

namespace BrainIndustries\Core\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Mail\TransportInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var WriteInterface
     */
    protected $_directory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_errorLogger = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger = null;

    /**
     * @var TransportInterface
     */
    protected $_mailTransport = null;

    /**
     * 
     * @param Context $context
     * @param \Psr\Log\LoggerInterface $errorLogger
     * @param Filesystem $filesystem
     * @param \Psr\Log\LoggerInterface $logger
     * @param TransportInterface $mailTransport
     */
    public function __construct(
            Context $context, 
            \Psr\Log\LoggerInterface $errorLogger,
            Filesystem $filesystem,
            \Psr\Log\LoggerInterface $logger,
            TransportInterface $mailTransport
    ) {
        parent::__construct($context);
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_errorLogger = $errorLogger;
        $this->_logger = $logger;
        $this->_mailTransport = $mailTransport;
    }

    /**
     * Get configuration value
     * 
     * @access public
     * @param string $path
     * @param int $storeId
     * @return string
     */
    public function getConfigValue($path, $storeId = null) {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Log message to plugin file log
     * 
     * @access public
     * @param mixed $message
     * @param int $priority
     * @param array|Traversable $extra
     */
    public function logMessage($message, $priority = \Monolog\Logger::INFO, $extra = []) {
        if (is_array($message) || is_object($message)){
            $this->_logger->log($priority, print_r($message, true), $extra);
        } else {
            $this->_logger->log($priority, $message, $extra);
        }
    }

    /**
     * Log error message to plugin file log
     * 
     * @access public
     * @param mixed $message
     * @param int $priority
     * @param array|Traversable $extra
     */
    public function logError($message, $priority = \Monolog\Logger::ERROR, $extra = []) {
        if (is_array($message) || is_object($message)){
            $this->_errorLogger->log($priority, print_r($message, true), $extra);
        } else {
            $this->_errorLogger->log($priority, $message, $extra);
        }
    }

    /**
     * @param string $string
     * @return string
     */
    public function removeAccents($string){
        return str_replace([
            'à','á','â','ã','ä','ç','ć','č','ď','đ','è','é','ê','ë','ě','ì','í','î','ï','ĺ','ľ','ň','ñ','ò','ó','ô','õ','ö','ő','ŕ','ř','ś','š','ť','ù','ú','û','ü','ů','ű','ý','ÿ','ž',
            'À','Á','Â','Ã','Ä','Ç','Ć','Č','Ď','Đ','È','É','Ê','Ë','Ě','Ì','Í','Î','Ï','Ĺ','Ľ','Ň','Ñ','Ò','Ó','Ô','Õ','Ö','Ő','Ŕ','Ř','Ś','Š','Ť','Ù','Ú','Û','Ü','Ů','Ű','Ý','Ÿ','Ž'
        ], [
            'a','a','a','a','a','c','c','c','d','d','e','e','e','e','e','i','i','i','i','l','l','n','n','o','o','o','o','o','o','r','r','s','s','t','u','u','u','u','u','u','y','y','z',
            'A','A','A','A','A','C','C','C','D','D','E','E','E','E','E','I','I','I','I','L','L','N','N','O','O','O','O','O','O','R','R','S','S','T','U','U','U','U','U','U','Y','Y','Z'
        ], $string);
    }

    /**
     * Send email to admin
     *      
     * @access public
     * @param string $subject
     * @param string $body
     * @param string|array $recipient email address or array of emails
     */
    public function sendEmailToAdmin($subject, $body, $recipient = null, $sender = null) {
        $sender = $sender ?? $this->getConfigValue('trans_email/ident_general/email');
        $recipient = $recipient ?? $sender;                
        $message = $this->_mailTransport->getMessage();        
        $message->setFrom($sender);
        $message->addTo($recipient);                        
        $message->setSubject($subject);
        $message->setBodyText($body);
        $this->_mailTransport->sendMessage();
    }

    /**
     * Get Request object
     * 
     * @access public
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->_getRequest();
    }

    /**
     * @param string $fileName
     * @param array $data
     * @return void
     */
    public function createCsvFile(string $fileName, array $data) : void
    {
        $stream = $this->_directory->openFile($fileName, 'w+');
        $stream->lock();

        $header = array_keys(current($data));
        $stream->writeCsv($header);

        foreach ($data as $_row) {
            $stream->writeCsv(array_values($_row));
        }

        $stream->unlock();
        $stream->close();
    }
}