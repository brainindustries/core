<?php

namespace BrainIndustries\Core\Model\ResourceModel\Order\Invoice\Grid;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{    
    /**
     * {@inheritdoc}
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        $this->join(
            ['order_address' => $this->getTable('sales_order_address')],
            'main_table.order_id = order_address.parent_id AND order_address.address_type = "shipping"',
            ['vat_id']
        );

        $this->addFilterToMap('has_vat_id', 'order_address.vat_id');
        return $this;
    }    
}